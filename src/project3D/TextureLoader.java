package project3D;

import java.io.File;
import java.io.IOException;

import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

/// A static class Thata encapsulate texture read from file
public class TextureLoader
{
    /**
     * Read a texture from a .jpg or png file
     * @param filePath The file path for the texture (.jpg or .png)
     * @return The texture loaded
     */
    public static Texture loadTexture(String filePath)
    {
        try
        {
            Texture texture = TextureIO.newTexture(new File(filePath), false);
            return texture;
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}
