package project3D;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import java.io.File;
import java.io.IOException;

import com.jogamp.opengl.util.gl2.GLUT;

import javax.media.opengl.awt.GLCanvas;

public class Scene implements GLEventListener
{	
	private GLU glu = new GLU();
	private GLUT glut = new GLUT();
    private Camera camera;
    private GLUquadric quad;

    private Entity ship = new Entity("./mesh/StarDestroyer.json");
    private Entity xWing = new Entity("./mesh/X-Wing.json");
    
    private Sphere sun = new Sphere("./texture/sun01.jpg", 1f, 16, 16, true);
    private Sphere mercury = new Sphere("./texture/planet06.png", 0.1f, 16, 16);
    private Sphere venus = new Sphere("./texture/planet12.jpg", 0.15f, 16, 16);
    private Sphere terre = new Sphere("./texture/planet11.jpg", 0.2f, 16, 16);
    private Sphere mars = new Sphere("./texture/planet04.jpg", 0.12f, 16, 16);
    private Sphere jupitere = new Sphere("./texture/planet09.png", 0.4f, 16, 16);
    private Sphere saturne = new Sphere("./texture/planet10.png", 0.34f, 16, 16);
    private Sphere uranus = new Sphere("./texture/planet02.jpg", 0.23f, 16, 16);
    private Sphere neptune = new Sphere("./texture/planet05.png", 0.26f, 16, 16);

    private SkyBox sky = new SkyBox("./texture/univers.jpg", 20f, 16, 16);

    private Sphere[] planetList = {sun, mercury, venus, terre, mars, jupitere, saturne, uranus, neptune};
    private float[] planetSpacing = {5f, 7.3f, 9.3f, -11.5f, -14f, -16.9f, 19.2f, 21.3f};
    private float[] selfRotateCoefs = {0.03f, 0.06f, 0.04f, 0.05f, 0.06f, 0.04f, 0.07f, 0.03f};
    private float[] selfRotateVal = {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f};
    private float[] solarRotateCoefs = {0.1f, 0.15f, 0.13f, 0.17f, 0.10f, 0.14f, 0.11f, 0.16f};
    private float[] solarRotateVal = {0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f};
    
    private float isocahedronRotation = 0.1f;
    private float isocahedronGravity = 0.1f;

    /**
     * Constructor that init camera
     * @param camera The scene camera
     */
    public Scene(Camera camera)
    {
        this.camera = camera;
    }

    @Override
    public void display(GLAutoDrawable drawable)
    {
        GL2 gl = drawable.getGL().getGL2();
	
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();

        // the camera lookAt
        this.camera.lookAt(glu);

        // glEnable for ligth
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glEnable(GL2.GL_NORMALIZE);
        gl.glEnable(GL2.GL_LIGHT0);
        gl.glShadeModel(GL2.GL_SMOOTH);

        float lightAmb[] = {1f, 1f, 1f, 1.0f};
        float lightDif[] = {1.0f, 1.0f, 1.0f, 1.0f};
        float lightSpe[] = {1.0f, 1.0f, 1.0f, 1.0f};
        float lightPos[] = {0, 0, 0, 1};

        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_AMBIENT, lightAmb, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_DIFFUSE, lightDif, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_SPECULAR, lightSpe, 0);
        gl.glLightfv(GL2.GL_LIGHT0, GL2.GL_POSITION, lightPos, 0);

        // draw:
        sky.draw(gl, glu);

        // Star Destroyers
        for (int i = 0; i < 6; ++i)
        {
            gl.glPushMatrix();
            int position = (i < 3) ? -i : i;
            gl.glTranslated(position, 2, -5);
            gl.glRotatef(1f, 1f, 0f, 0f);
            gl.glScalef(0.05f, 0.05f, 0.05f);
            ship.draw(gl);
            gl.glPopMatrix();
        }

        // X-Wing
        gl.glPushMatrix();
        gl.glTranslated(0, 2, 5);
        gl.glRotatef(180f, 0f, 1f, 0f);
        gl.glScalef(0.01f, 0.01f, 0.01f);
        xWing.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslated(0.5f, 2, 5);
        gl.glRotatef(25f, 0f, 0f, 1f);
        gl.glRotatef(180f, 0f, 1f, 0f);
        gl.glScalef(0.01f, 0.01f, 0.01f);
        xWing.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslated(-0.5f, 2, 5);
        gl.glRotatef(-25f, 0f, 0f, 1f);
        gl.glRotatef(180f, 0f, 1f, 0f);
        gl.glScalef(0.01f, 0.01f, 0.01f);
        xWing.draw(gl);
        gl.glPopMatrix();

        // Sun
        gl.glPushMatrix();
        gl.glRotatef(selfRotateVal[0], 0f, 1f, 0f);
		sun.drawSphere(gl, glu);
        gl.glPopMatrix();

        // planets and transformation
        for (int i = 1; i < planetList.length; i++)
        {
            gl.glPushMatrix();
            gl.glRotatef(solarRotateVal[i-1], 0f, 1f, 0f);

            if (i % 3 == 1)
            {
                gl.glTranslated(planetSpacing[i-1], 0, 0);
            }
            else if (i % 3 == 2)
            {
                gl.glTranslated(0, 0, planetSpacing[i-1]);
            }
            else
            {
                gl.glTranslated((planetSpacing[i -1] * 0.707), 0,
                                (planetSpacing[i -1] * 0.707));
            }
            gl.glRotatef(selfRotateVal[i], 0f, 1f, 0f);
            planetList[i].drawSphere(gl, glu);

            if (i == 3)
            {
                // Icosahedron
                isocahedronGravity += 0.6f;
                gl.glRotatef(isocahedronGravity, 0f, 1f, 0f);
                gl.glTranslated(0f, 0f, 0.5f);

                gl.glScalef(0.1f, 0.1f, 0.1f);

                gl.glPushMatrix();
                isocahedronRotation += 1f;
                gl.glRotatef(isocahedronRotation, 0f, -1f, 0f);
                glut.glutSolidIcosahedron();
                gl.glPopMatrix();
            }

            gl.glPopMatrix();
        }

        // planets rotation
        for (int i=0; i < selfRotateCoefs.length; i++)
            selfRotateVal[i] += selfRotateCoefs[i];

        // sun rotation
        for (int i=0; i < solarRotateCoefs.length; i++)
            solarRotateVal[i] += solarRotateCoefs[i];
        
        gl.glFlush();
	}

    @Override
    public void dispose(GLAutoDrawable drawable)
    { 
    }

    @Override
    public void init(GLAutoDrawable drawable)
    {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor (0f, 0f, 0f, 1.0f);
        gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glClearDepth(1.0f);
	}

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                        int height)
    {
		GL2 gl = drawable.getGL().getGL2();

        gl.glViewport (0, 0, (int) width, height); 
        gl.glMatrixMode (GL2.GL_PROJECTION);
        gl.glLoadIdentity ();
        glu.gluPerspective(70.0,  width/(float) height, 0.01, 30.0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

    void fleche(GLAutoDrawable drawable,float t1,float t2,float t3,float taille,
                char dim)
    {
        GL2 gl = drawable.getGL().getGL2();

        switch(dim)
        {
        case 'x':
            gl.glBegin(GL2.GL_LINE_STRIP);
            gl.glVertex3f (t1-taille, t2+taille, t3);
            gl.glVertex3f (t1, t2, t3);
            gl.glVertex3f (t1-taille, t2-taille, t3);
            gl.glEnd();
            break;
        case 'y':
            gl.glBegin(GL2.GL_LINE_STRIP);
            gl.glVertex3f (t1, t2-taille, t3+taille);
            gl.glVertex3f (t1, t2, t3);
            gl.glVertex3f (t1, t2-taille, t3-taille);
            gl.glEnd();
            break;
        case 'z':
            gl.glBegin(GL2.GL_LINE_STRIP);
            gl.glVertex3f (t1, t2+taille, t3-taille);
            gl.glVertex3f (t1, t2, t3);
            gl.glVertex3f (t1, t2-taille, t3-taille);
            gl.glEnd();
            break;
        default: break;
        }
    }    
    
    void repere(GLAutoDrawable drawable,float t)
    {
        GL2 gl = drawable.getGL().getGL2();

        float coef=1;
        gl.glPushAttrib(GL2.GL_LIGHTING);
        
        gl.glDisable(GL2.GL_LIGHTING);
        
        gl.glLineWidth(1.0f);
        gl.glBegin(GL2.GL_LINES);
        gl.glColor3f(1.0f,0.0f,0.0f);
        gl.glVertex3f (t, 0.0f, 0.0f);
        gl.glVertex3f (-t, 0.0f, 0.0f);
        gl.glColor3f(0.0f,1.0f,0.0f);
        gl.glVertex3f ( 0.0f, 0.0f, coef*t);
        gl.glVertex3f ( 0.0f, 0.0f, -t*coef);
        gl.glColor3f(0.0f,1.0f,1.0f);
        gl.glVertex3f ( 0.0f, t, 0.0f);
        gl.glVertex3f ( 0.0f, -t, 0.0f);
        gl.glEnd();
        gl.glColor3f(1.0f,0.0f,0.0f);
        fleche(drawable,t, 0.0f, 0.0f,t*0.1f ,'x');
        gl.glColor3f(0.0f,1.0f,0.0f);
        fleche(drawable,0.0f, 0.0f, coef*t,t*0.1f ,'z');
        gl.glColor3f(0.0f,1.0f,1.0f);
        fleche(drawable,0.0f, t, 0.0f,t*0.1f ,'y');
        gl.glLineWidth(0.1f);
        gl.glPopAttrib();
    }
}
