package project3D;

import javax.media.opengl.GL2;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.Buffer;
import java.nio.IntBuffer;
import java.nio.DoubleBuffer;
import com.jogamp.common.nio.Buffers;

/// Mesh displayer
public class Entity
{
    private boolean isVBOInit = false;

    // VBO indexes
    private int verticesVBO;
    private int indexVBO;
    private int normalVBO;

    // buffers
    private DoubleBuffer verticesBuff;
    private DoubleBuffer normalBuff;
    private IntBuffer connection;

    /**
     * Constructor that load the json model store in the file
     * @param file The file where the mesh is store
     */
    public Entity(String file)
    {
        String jsonString="";

        try
        {
            jsonString = new Scanner(new File(file)).useDelimiter("\\Z").next();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        try
        {
            // Vertex
            JSONObject object = new JSONObject(jsonString);
            JSONArray arr = object.getJSONArray("vertices");
            JSONArray arr2 = arr.getJSONObject(0).getJSONArray("values");

            double vertices[] = new double[arr2.length()];
            for (int i = 0; i < arr2.length(); i++)
                vertices[i] = arr2.getDouble(i);
            verticesBuff = Buffers.newDirectDoubleBuffer(vertices);

            // Normal
            JSONArray normalValues = arr.getJSONObject(1)
                .getJSONArray("values");

            double normal[] = new double[normalValues.length()];
            for (int i = 0; i < normal.length; ++i)
                normal[i] = normalValues.getDouble(i);
            normalBuff = Buffers.newDirectDoubleBuffer(normal);

            // Connectivity
            JSONArray arrConnectivity = object.getJSONArray("connectivity");
            JSONArray arrIndices = arrConnectivity.getJSONObject(0)
                .getJSONArray("indices");

            int connectivity[] = new int[arrIndices.length()];
            for (int i = 0; i < arrIndices.length(); ++i)
                connectivity[i] = arrIndices.getInt(i);
            connection = Buffers.newDirectIntBuffer(connectivity);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Single called method that init the VBO and bind vertices, index and
     * normal buffers
     * @param gl The openGL context
     */
    private void init(GL2 gl)
    {
        if (!isVBOInit)
        {
            int temps[] = new int[3];
            gl.glGenBuffers(3, temps, 0);

            verticesVBO = temps[0];
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, verticesVBO);
            gl.glBufferData(GL2.GL_ARRAY_BUFFER, verticesBuff.capacity() *
                            Buffers.SIZEOF_DOUBLE, verticesBuff,
                            GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

            indexVBO = temps[1];
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, indexVBO);
            gl.glBufferData(GL2.GL_ARRAY_BUFFER, connection.capacity() *
                            Buffers.SIZEOF_INT, connection,
                            GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

            normalVBO = temps[2];
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, normalVBO);
            gl.glBufferData(GL2.GL_ARRAY_BUFFER, normalBuff.capacity() *
                            Buffers.SIZEOF_DOUBLE, normalBuff,
                            GL2.GL_STATIC_DRAW);
            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

            gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);

            isVBOInit = true;
        }
    }

    /**
     * Draws the mesh with its normals
     * @param gl The openGL context
     */
    public void draw(GL2 gl)
    {
        init(gl);

        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);

        // vertex
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, verticesVBO);
        gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, 0);

        // normal
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, normalVBO);
        gl.glNormalPointer(GL2.GL_DOUBLE, 0, 0);

        // index
        gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, indexVBO);
        gl.glDrawElements(GL2.GL_TRIANGLES, connection.capacity(),
                          GL2.GL_UNSIGNED_INT, 0);

        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);

        // unbind
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL2.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    /**
     * Draw mesh without VBO use
     * @param gl The openGL context
     */ 
    public void drawFromJsonWithoutVBO(GL2 gl)
    {
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glVertexPointer(3, GL2.GL_DOUBLE, 0, verticesBuff);

        gl.glDrawElements(GL2.GL_TRIANGLES, connection.capacity(),
                          GL2.GL_UNSIGNED_INT, connection);

        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
    }
}
