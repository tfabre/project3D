package project3D;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.texture.Texture;

/// Entity that represent sphere
public class SkyBox
{
    GLUquadric quad;
    String texturePath;
    Texture texture;
    float radius;
    int slices;
    int stacks;

    /**
     * Classic constructor
     * @param texturePath The file path for the texture
     * @param radius The sphere radius
     * @param slices The sphere slice
     * @param stacks The sphere stacks
     */
    public SkyBox(String texturePath, float radius, int slices, int stacks)
    {
        this.texturePath = texturePath;
        this.radius = radius;
        this.slices = slices;
        this.stacks = stacks;
    }

    /**
     * Single called function to load the texture and set the sphere parameters
     * @param gl The openGL context
     * @param glu The glu context
     */
    public void loadTexture(GL2 gl, GLU glu)
    {
        if (!texturePath.equals(""))
        {
            this.texture = TextureLoader.loadTexture(texturePath);
            this.quad = glu.gluNewQuadric();
            glu.gluQuadricDrawStyle(this.quad, GLU.GLU_FILL);
            glu.gluQuadricNormals(this.quad, GLU.GLU_SMOOTH);
            glu.gluQuadricTexture(this.quad, true);

            texturePath = "";
        }
    }

    /**
     * Draw the sphere in the scene
     * @param gl The openGL context
     * @param glu The glu context
     */
    public void draw(GL2 gl, GLU glu)
    {
        loadTexture(gl, glu);

        // gl.glMaterialfv(GL2.GL_BACK, GL2.GL_EMISSION, this.material, 0);
        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE,
                     GL2.GL_MODULATE);

        this.texture.enable(gl);
        this.texture.bind(gl);

        glu.gluSphere(quad, radius, slices, stacks);

        gl.glDisable(GL2.GL_TEXTURE_2D);
    }
}
