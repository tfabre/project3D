package project3D;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import com.jogamp.opengl.util.FPSAnimator;
import project3D.math.Vec3f;

public class Main extends JFrame
{
    public Main()
    {
		setTitle("project3D");
        setSize(500, 500);
        // setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final GLCanvas canvas = new GLCanvas();
        final Camera camera = new Camera(new Vec3f(0, 0, 10), new Vec3f(0, 0, 0), this);
        final Scene scene = new Scene(camera);

		canvas.addGLEventListener(scene);
        canvas.addMouseMotionListener(camera);
        this.getContentPane().add(canvas);

        final FPSAnimator anim = new FPSAnimator(canvas,60);
        anim.start();

        this.addKeyListener(camera);

		System.out.print("apres listener---- \n");
	}

	public static void main(String [] args)
    {
		(new Main()).setVisible(true);
	}
}
