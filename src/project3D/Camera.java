package project3D;

import java.awt.Point;
import java.awt.MouseInfo;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JFrame;
import javax.media.opengl.glu.GLU;

import project3D.math.Vec3f;

/// 3D Camera
public class Camera implements KeyListener, MouseMotionListener
{
    private JFrame frame;
    private boolean isCoorInit = false;

    // movement activation
    private boolean up;
    private boolean down;
    private boolean moveLeft;
    private boolean moveRight;

    // booleans to handle the lookAt
    private boolean right;
    private boolean left;
    private boolean stop = false;

    // mouse initial coordinates
    private int x = 250;
    private int y = 250;

    //  move speed
    private float velocity = 0.05f;
    private float sensitivity = 0.5f;

    // information for camera
    private float phi;
    private float theta;
    private Vec3f orientation;
    private Vec3f axeY = new Vec3f(0, 1, 0);
    private Vec3f normal = new Vec3f(1, 0, 0);
    private Vec3f position;
    private Vec3f target;

    /**
     * The constructor
     * @param position The camera intial position
     * @param target The camera focus target
     * @param frame The application context
     * @see Vec3f
     */
    public Camera(Vec3f position, Vec3f target, JFrame frame)
    {
        this.frame = frame;
        this.position = position;
        this.target = target;

        this.orientation = Vec3f.norm(Vec3f.dif(this.target, this.position));

        this.phi = (float)Math.asin(this.orientation.getY());
        this.theta = (float)Math.acos(this.orientation.getZ() /
                                      Math.cos(this.phi));

        if (this.orientation.getZ() < 0)
            this.theta *= -1;

        this.phi = (float)Math.toDegrees(this.phi);
        this.theta = (float)Math.toDegrees(this.theta);
    }

    /**
     * Set the gluLookAt with the computed values
     * @param glu The glu context
     */
    public void lookAt(GLU glu)
    {
        move();
        if (right || left)
            autoLookAt();
        glu.gluLookAt(position.getX(), position.getY(), position.getZ(),
                      target.getX(), target.getY(), target.getZ(),
                      axeY.getX(), axeY.getY(), axeY.getZ());
    }

    @Override
    public void keyTyped(KeyEvent event) {}

    /**
     * Methode that disable the movement
     * @param event The event that trigger the camera move
     */
    @Override
    public void keyReleased(KeyEvent event)
    {
        int key = event.getKeyCode();
        if (key == KeyEvent.VK_KP_UP || key == KeyEvent.VK_Z)
            up = false;
    
        if (key == KeyEvent.VK_KP_DOWN || key == KeyEvent.VK_S)
            down = false;

        if (key == KeyEvent.VK_KP_LEFT || key == KeyEvent.VK_Q)
            moveLeft = false;

        if (key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_D)
            moveRight = false;
    }

    /**
     * Methode that enable the movement
     * @param event The event that trigger the camera move
     */
    @Override
    public void keyPressed(KeyEvent event)
    {
        int key = event.getKeyCode();

        if (event.getKeyCode() == KeyEvent.VK_SPACE)
            stop = (stop) ? false : true;

        if (key == KeyEvent.VK_KP_UP || key == KeyEvent.VK_Z)
            up = true;

        if (key == KeyEvent.VK_KP_DOWN || key == KeyEvent.VK_S)
            down = true;

        if (key == KeyEvent.VK_KP_LEFT || key == KeyEvent.VK_Q)
            moveLeft = true;

        if (key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_D)
            moveRight = true;
    }

    /**
     * Compute the new camera position in function of direction enabled
     */
    public void move()
    {
        if (up)
        {
            this.position = Vec3f.sum(this.position,
                                      Vec3f.mul(this.orientation,
                                                this.velocity));
            this.target = Vec3f.sum(this.position, this.orientation);
        }
        if (down)
        {
            this.position = Vec3f.dif(this.position,
                                      Vec3f.mul(this.orientation,
                                                this.velocity));

            this.target = Vec3f.sum(this.position, this.orientation);

        }
        if (moveLeft)
        {
            this.position = Vec3f.sum(this.position,
                                      Vec3f.mul(this.normal, this.velocity));
            this.target = Vec3f.sum(this.position, this.orientation);
        }
        if (moveRight)
        {
            this.position = Vec3f.dif(this.position,
                                      Vec3f.mul(this.normal, this.velocity));
            this.target = Vec3f.sum(this.position, this.orientation);
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {}

    /**
     * Initialized the originial position for the camera focus target
     * Use it only if you don't want to look at in the middle of the application
     * @param event The first move event
     */
    private void initCoord(MouseEvent event)
    {
        if (!this.isCoorInit)
        {
            this.x = event.getX();
            this.y = event.getY();
            this.isCoorInit = true;
        }
    }

    /**
     * Compute the new camera focus taget in function of the mouse move
     * @param event The mouse move
     */
    @Override
    public void mouseMoved(MouseEvent event)
    {
        // initCoord(event);

        int eX = event.getX();
        int eY = event.getY();

        if (eX > 475)
            right = true;

        else if (eX < 25)
            left = true;

        else
            right = left = false;

        cameraLookAt(x - eX, y - eY);

        x = eX;
        y = eY;

    }

    /**
     * Set the lookAt coordinate automaticly when user mouse is on the border of
     * the application
     */
    public void autoLookAt()
    {
        int selfX = 0;
        int selfY = 0;

        if (left)
            selfX++;
        else if (right)
            selfX--;
        cameraLookAt(selfX, 0);
    }

    /**
     * Set the lookAt in function of coodinates given in parameters
     * @param selfX The x coordinate
     * @param selfY The y coordinate
     */
    private  void cameraLookAt(int selfX, int selfY)
    {
        if (!stop)
        {
            this.phi += selfY * this.sensitivity;
            this.theta += selfX * this.sensitivity;
        }

        if (this.phi > 89f)
            this.phi = 89f;
        else if (this.phi < -89f)
            this.phi = -89f;

        float phiRad = (float)Math.toRadians(phi);
        float thetaRad = (float)Math.toRadians(theta);

        this.orientation.set((float)Math.cos(phiRad)* (float)Math.sin(thetaRad),
                             (float)Math.sin(phiRad),
                             (float)Math.cos(phiRad)*(float)Math.cos(thetaRad));

        this.normal = Vec3f.norm(Vec3f.dot(this.axeY, this.orientation));
        this.target = Vec3f.sum(this.position, this.orientation);
    }
}
