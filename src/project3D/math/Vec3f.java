package project3D.math;

/**
 * Class to handle Vectors composed of 3 float
 */
public class Vec3f
{
    private float x;
    private float y;
    private float z;

    /**
     * Constructor that initialize the 3 attributes of the vector
     * @param x The x attribute
     * @param y The y attribute
     * @param z The z attribute
     */
    public Vec3f(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Set the attribute with the given parameters
     * @param x The x attribute
     * @param y The y attribute
     * @param z The z attribute
     */
    public void set(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Set the x attribute
     * @param x The x value
     */
    public void setX(float x) { this.x = x; }
    
    /**
     * Set the y attribute
     * @param y The y value
     */
    public void setY(float y) { this.y = y; }

    /**
     * Set the z attribute
     * @param z The z value
     */
    public void setZ(float z) { this.z = z; }

    /**
     * Return the x attribute
     * @return The x attribute
     */
    public float getX() { return this.x; }

    /**
     * Return the y attribute
     * @return The y attribute
     */
    public float getY() { return this.y; }
    
    /**
     * Return the z attribute
     * @return The z attribute
     */
    public float getZ() { return this.z; }

    /**
     * Compute the vector given in paramter norm
     * @param vec The vector
     * @return The normalized vector
     */
    public static Vec3f norm(Vec3f a)
    {
        float length = (float)Math.sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
        return new Vec3f(a.x / length, a.y / length, a.z / length);
    }

    /**
     * Compute the sum between the 2 vectors given in parameter
     * @param a The first vector
     * @param b The second vector
     * @return The sum of the 2 vectors
     */
    public static Vec3f sum(Vec3f a, Vec3f b)
    {
        return new Vec3f(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    /**
     * Compute the difference between the 2 vectors given in parameter
     * @param a The first vector
     * @param b The second vector
     * @return The difference of the 2 vectors
     */
    public static Vec3f dif(Vec3f a, Vec3f b)
    {
        return new Vec3f(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    /**
     * Compute the scalare product between the 2 vectors given in parameter
     * @param a The first vector
     * @param b The second vector
     * @return The scalare product of the 2 vectors
     */
    public static Vec3f dot(Vec3f a, Vec3f b)
    {
        return new Vec3f(
            (a.y * b.z) - (a.z * b.y),
            (a.z * b.x) - (a.x * b.z),
            (a.x * b.y) - (a.y * b.x));
    }

    /**
     * Compute the product between the vector and the float value given in
     * parameter
     * @param a The first vector
     * @param val The float value
     * @return The product of the vector and the value
     */
    public static Vec3f mul(Vec3f a, float val)
    {
        return new Vec3f(a.x * val, a.y * val, a.z * val);
    }
}
